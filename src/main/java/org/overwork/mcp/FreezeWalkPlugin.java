package org.overwork.mcp;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.plugin.java.JavaPlugin;

public class FreezeWalkPlugin extends JavaPlugin implements Listener {

	@Override
	public void onEnable() {
		getServer().getPluginManager().registerEvents(this, this);
	}

	@EventHandler
	public void onPlayerMove(PlayerMoveEvent event) {
		Location center = event.getTo().subtract(0, 1, 0);
		int x = center.getBlockX();
		int y = center.getBlockY();
		int z = center.getBlockZ();

		World world = event.getPlayer().getWorld();
		freezeWaterBlock(world.getBlockAt(x + 0, y + 0, z + 0));
		freezeWaterBlock(world.getBlockAt(x + 0, y - 1, z + 0));
		freezeWaterBlock(world.getBlockAt(x - 1, y + 0, z + 0));
		freezeWaterBlock(world.getBlockAt(x + 1, y + 0, z + 0));
		freezeWaterBlock(world.getBlockAt(x + 0, y + 0, z - 1));
		freezeWaterBlock(world.getBlockAt(x + 0, y + 0, z + 1));
	}

	private void freezeWaterBlock(Block block) {
		if (block.getType() == Material.STATIONARY_WATER)
			block.setType(Material.ICE);
	}
}
